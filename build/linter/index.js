"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
const eslint_1 = require("eslint");
const baseConfig = require("./base-config");
const paths_1 = require("../paths");
const linter = new eslint_1.CLIEngine({
    useEslintrc: false,
    globals: ['REACT_APP_ENV', 'fetch', 'window', 'document'],
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
    baseConfig
});
exports.run = (locs = []) => {
    const destinations = locs.length ? locs.map((l) => path_1.resolve(paths_1.default.appRoot, l)) : [paths_1.default.appSrc];
    // eslint-disable-next-line no-console
    console.log(`Linting all [java|type]script in ${destinations.join(', ')}.`);
    const report = linter.executeOnFiles(destinations);
    const formatter = linter.getFormatter('table');
    // eslint-disable-next-line no-console
    console.log(formatter(report.results));
    return report;
};
exports.eject = () => {
    const config = {
        extends: path_1.resolve(__dirname, './base-config.js')
    };
    const dependencies = [
        'eslint',
        'babel-eslint',
        'eslint-plugin-import',
        'eslint-plugin-react',
        'typescript-eslint-parser',
        'eslint-restricted-globals'
    ];
    const configStr = JSON.stringify(config, null, '  ');
    const configPath = path_1.resolve(paths_1.default.appRoot, '.eslintrc');
    /* eslint-disable no-console */
    fs_1.writeFile(configPath, configStr, (err) => {
        if (err) {
            console.log(`Failed to create ${configPath}`);
            console.error(err);
            return;
        }
        console.log(`Created ${configPath}.`);
        console.log('\nPlease run following command to install dependencies:');
        console.log(`\n\nyarn add -D ${dependencies.join(', ')}`);
    });
    /* eslint-enable */
};
//# sourceMappingURL=index.js.map