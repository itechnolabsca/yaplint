"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const appRoot = process.env.APP_ROOT || process.cwd();
const paths = {
    appRoot,
    appSrc: path_1.resolve(appRoot, 'src')
};
exports.default = paths;
//# sourceMappingURL=paths.js.map