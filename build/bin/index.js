#! /usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const yargs_1 = require("yargs");
const linter_1 = require("../linter");
// Default command
// Runs linter over all the files in given directories
yargs_1.command('*', 'Run lint on given files/directories');
yargs_1.options({
    eject: {
        describe: 'Eject eslint rules used by yaplint to .eslintrc',
        boolean: true,
        default: false
    }
});
const argv = yargs_1.parse();
const runCli = ({ targets, eject }) => {
    if (eject) {
        linter_1.eject();
        return;
    }
    if (targets.length) {
        linter_1.run(targets);
        return;
    }
    linter_1.run(targets);
};
runCli({
    targets: argv._,
    eject: argv.eject
});
//# sourceMappingURL=index.js.map