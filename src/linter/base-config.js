const { resolve } = require('path');

module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },
  env: {
    browser: true,
    node: true
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx']
      }
    }
  },
  extends: [
    resolve(__dirname, './rules/best-practices.js'),
    resolve(__dirname, './rules/errors.js'),
    resolve(__dirname, './rules/es6.js'),
    resolve(__dirname, './rules/imports.js'),
    resolve(__dirname, './rules/strict.js'),
    resolve(__dirname, './rules/style.js'),
    resolve(__dirname, './rules/variables.js'),
    resolve(__dirname, './rules/node.js'),
    resolve(__dirname, './rules/react.js')
  ],
  overrides: [
    {
      files: ['**/*.ts', '**/*.tsx'],
      parser: 'typescript-eslint-parser'
    }
  ]
};
