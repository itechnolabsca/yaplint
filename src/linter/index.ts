import { writeFile } from 'fs';
import { resolve } from 'path';
import { CLIEngine } from 'eslint';
import * as baseConfig from './base-config';

import paths from '../paths';

const linter = new CLIEngine({
  useEslintrc: false,
  globals: ['REACT_APP_ENV', 'fetch', 'window', 'document'],
  extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
  baseConfig
});

export const run = (locs = []) => {
  const destinations = locs.length ? locs.map((l) => resolve(paths.appRoot, l)) : [paths.appSrc];

  // eslint-disable-next-line no-console
  console.log(`Linting all [java|type]script in ${destinations.join(', ')}.`);
  const report = linter.executeOnFiles(destinations);
  const formatter = linter.getFormatter('table');

  // eslint-disable-next-line no-console
  console.log(formatter(report.results));

  return report;
};

export const eject = () => {
  const config = {
    extends: resolve(__dirname, './base-config.js')
  };
  const dependencies = [
    'eslint',
    'babel-eslint',
    'eslint-plugin-import',
    'eslint-plugin-react',
    'typescript-eslint-parser',
    'eslint-restricted-globals'
  ];

  const configStr = JSON.stringify(config, null, '  ');
  const configPath = resolve(paths.appRoot, '.eslintrc');

  /* eslint-disable no-console */
  writeFile(configPath, configStr, (err) => {
    if (err) {
      console.log(`Failed to create ${configPath}`);
      console.error(err);
      return;
    }

    console.log(`Created ${configPath}.`);
    console.log('\nPlease run following command to install dependencies:');
    console.log(`\n\nyarn add -D ${dependencies.join(', ')}`);
  });
  /* eslint-enable */
};
