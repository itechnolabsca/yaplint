#! /usr/bin/env node

import { command, options, parse } from 'yargs';
import { resolve } from 'path';
import paths from '../paths';
import { run, eject as ejectEslint } from '../linter';

// Default command
// Runs linter over all the files in given directories
command('*', 'Run lint on given files/directories');
options({
  eject: {
    describe: 'Eject eslint rules used by yaplint to .eslintrc',
    boolean: true,
    default: false
  }
});

const argv = parse();

const runCli = ({ targets, eject }) => {
  if (eject) {
    ejectEslint();
    return;
  }

  if (targets.length) {
    run(targets);
    return;
  }

  run(targets);
};

runCli({
  targets: argv._,
  eject: argv.eject
});
