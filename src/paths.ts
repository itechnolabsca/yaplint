import { resolve } from 'path';

const appRoot = process.env.APP_ROOT || process.cwd();

const paths = {
  appRoot,
  appSrc: resolve(appRoot, 'src')
};

export default paths;
